---

title: "Global Digital SMB"
description: "Overview of the Global Digital SMB Sales Model"
---

## Global Digital SMB Sales Model

All members of the Sales, Support, Billing and Deal Desk teams should familiarize themselves with the Global Digital SMB Model.
The model leverages team-level account alignment so SMB customers have a team of SMB Advocates to assist them. Every SMB Advocate on the team is equipped to work with any SMB customer as the sales point of contact.
The threshold for Global Digital SMB Accounts accounts is <$30k cARR, <100 employees, and excludes Ultimate Licenses and Japan. 


## Account and Opportunity Ownership

Vision: SMB Accounts will not be owned individually, but by the entire team. This is meant to ensure that SMB customers are supported at the right time, in a scalable way. Each SMB Advocate will have a shared responsibility for collaborating in a customer-centric  manner to provide a best-in-class experience to each SMB customer. 

### Accounts

 - All AMER SMB Accounts are owned by the AMER SMB Sales User.
 - All APAC SMB Accounts are owned by the APAC SMB Sales User.
 - All EMEA SMB Accounts are owned by the EMEA SMB Sales User. 
 - Note that in SMB only, AMER & APAC are considered one territory.

### Opportunities 

 - All AMER SMB Opportunities are owned by the AMER SMB Sales User.
 - All APAC SMB Opportunities are owned by the APAC SMB Sales User.
 - All EMEA SMB Opportunities are owned by the EMEA SMB Sales User. 


### High Value Accounts

Some Global Digital SMB Accounts are considered to be Tier 1/ High Value Accounts,  based on their cARR and LAM. These accounts, whilst owned by an SMB Sales User, will be managed in a 1:1 relationship between the SMB Advocate and the customer, via Cases. 

The criteria that determines whether an SMB account is a Tier 1 account or not, can be found below. The table also highlights what type of Cases will be created for SMB Accounts, based on their respective Tier.

| Tier | Criteria                                                 | Digital Touch | High Value Cases | High Value 90/180/270 checkins (fallback)   | Urgent Renewal & TRX Support Case | Churn & Contraction Mitigation Cases | Expansion Opportunities Cases | Inbound Request Cases |
|------|----------------------------------------------------------|---------------|------------------|---------------------------------------------|-----------------------------------|--------------------------------------|-------------------------------|-----------------------|
| 1    | cARR > $7k                                               | Yes           | Yes              | Yes (if no other engagement within quarter) | Yes                               | Yes                                  | Yes                           | Yes                   |
| 2    | cARR < $7k AND (cARR > $1k OR (cARR < $1k AND LAM > 10)) | Yes           | No               | No                                          | Yes                               | Yes                                  | Yes                           | Yes                   |
| 3    | cARR < $1k AND LAM < 10                                  | Yes           | No               | No                                          | Yes                               | No                                   | No                            | Yes                   |

## Engaging with Global Digital SMB Accounts

Since all Global Digital & SMB Accounts are owned by generic SMB Sales Users, SMB Advocates will only engage with customers when specific customer events/ scenarios are triggered. Once one of these defined scenarios is triggered, a case will auto be created. An SMB Advocate will then pick up the case, and work it through to completion.

#### Why?
- Enables Advocates to focus on those SMB customers that require, or would benefit from, sales assistance.
- Removes low potential customers from view, since these customers will seldom trigger a case. 

#### How?
Cases are automatically created when;

- A customer requires sales assistance in order to transact.
- There is a high likelihood a customer will churn/ contract.
- There is a high likelihood that a customer is considering upgrading.
- If none of the above apply, and the customer is on auto renew, a case will NOT be created.

## Case Types

The cases that will be auto created, are split into 5 distinct categories;

Inbound Request - These occur when a customer hand raises, and requests assistance from GitLab.  The cases are labeled as High Priority. 
- Contact Sales Request
- Hand-Raise PQL
- Support Ticket
- SDR Created

Churn & Contraction Mitigation - These occur when the account in question has exhibited signs of low usage/ adoption. The cases are labeled as Medium Priority. 
- Underutilization
- High PtC
- Auto Renew recently turned off

Expansion Opportunities - These occur when the account in question has exhibited signs of growth, and that they are likely to expand. The cases are labeled as Medium Priority.  
- FO Opp
- FO Opp (Startup)
- High PtE
- 6Sense Growth Signal
- Overage with QSRs turned off
- Customer MQL

Urgent Renewal & TRX Support - These are created when a situation exists that means the renewal must be processed by an Advocate. The cases are labeled as Medium Priority. 
- PO Renewal (includes partner & alliance renewals)
- EoA Renewal w/ >25 users
- Multiyear Renewal
- Auto Renewal due to fail
- Overdue Renewals

High Value - These are only created for the highest spending accounts in the segment. 
- High Value Account
- High Value Account check in
- Future High Value Account


The case logic, context behind the case, and CTA can be viewed on the Case Trigger overview sheet. 

## Working with the Global Digital SMB Account Team

If a GitLab team member needs to loop in the Advocate team on a customer Account, they must create a case.  (Chatter messages sent to the AMER/ APAC/ EMEA Sales Users are not monitored).  

- Navigate to the *Account* in Salesforce.
- Click on *Cases*, New.
- Select *SMB Sales Case*, from the Record Type dropdown.
- Add the *Contact*, to the Contact Name lookup field.
- Add the *Opportunity*, to the Opportunity lookup field (if relevant). 
- Describe the ask of the AE in the case *Description*. Include any relevant links or resources.
- Select a *Type* that most closely matches the origin of your request. 
- Set the *Priority* to High. 
- Complete the *Subject* field.
- Select a *Case Reason* based on the customers needs.
- Check the *Assign using active assignment rules* checkbox, and click *Save*.
- This Case will now drop into the SMB Sales Queue, and will be actioned by a SMB Advocate.

