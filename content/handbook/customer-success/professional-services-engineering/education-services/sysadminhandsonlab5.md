---
title: "GitLab System Administration - Hands-on Lab 5"
description: "This hands-on lab guide is designed to walk you through the lab exercises used in the GitLab System Administration course."
---

> Estimated time to complete: 30 minutes

> **You are viewing the latest Version 16.x instructions.** If you are using `https://spt.gitlabtraining.cloud`, please use the [Version 15.x instructions](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/d14ee71aeac2054c72ce96e8b35ba2511f86a7ca/content/handbook/customer-success/professional-services-engineering/education-services/sysadminhandson5.md).

## Lab 5 - Implement Sign-Up Restrictions

This lab will help you improve your instance's security by enabling the option to send a confirmation email on signup, and ensure that signups are only allowed from your company's domain. 

1. Log into your GitLab web instance with your `root` user and password from Lab 1.

1. In the bottom left corner of the main screen in the sidebar, click **Admin Area**.

1. In the bottom of the left hand side navigation pane and click **Settings > General**.

1. Under **Sign-up restrictions**, click **Expand**.  

1. Under **Email confirmation settings**, click the radio button next to **Hard**.

1. Next, ensure sign ups are only allowed for your company's domain. In the **Allowed domains for sign-ups**, type your company’s domain name and press <kbd>Enter<kbd>.  

1. Scroll down to the end of the section and click **Save Changes**. 

## Lab Guide Complete

You have completed this lab exercise. You can view the other [lab guides for this course](/handbook/customer-success/professional-services-engineering/education-services/sysadminbhandson).

### Suggestions?

If you’d like to suggest changes to the GitLab System Admin Basics Hands-on Guide, please submit them via merge request.

