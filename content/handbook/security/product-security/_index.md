---
title: "Product Security"
aliases:
- /handbook/security/security-engineering
---

## Product Security Sub-Department

The Product Security sub-department is responsible for technical and engineering security specific to the GitLab product and internal used systems or applications.

### Teams

The Product Security sub-department includes the following teams. Learn more about each by visiting their Handbook pages.

- [Application Security]({{< ref "./application-security" >}})
- [Infrastructure Security]({{< ref "./infrastructure-security" >}})
- [Product Security Engineering]({{< ref "./product-security-engineering" >}})
